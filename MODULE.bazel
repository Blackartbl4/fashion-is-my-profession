###############################################################################
# Bazel now uses Bzlmod by default to manage external dependencies.
# Please consider migrating your external dependencies from WORKSPACE to MODULE.bazel.
#
# For more details, please check https://github.com/bazelbuild/bazel/issues/18958
###############################################################################
# bazel_dep(name = "rules_go", version = "0.46.0")
bazel_dep(name = "rules_cc", version = "0.0.9")
bazel_dep(name = "rules_python", version = "0.31.0")
bazel_dep(name = "platforms", version = "0.0.9")
bazel_dep(name = "bazel_skylib", version = "1.5.0")
bazel_dep(name = "rules_foreign_cc", version = "0.10.1")

# tools = use_extension("@rules_foreign_cc//foreign_cc:extensions.bzl", "tools")
# tools.cmake(version = "3.23.1")
# tools.ninja(version = "1.11.1")
# use_repo(
#     tools,
#     "meson_src",
# )

# bazel_dep(name = "rules_swift", version = "1.6.0", repo_name = "build_bazel_rules_swift")
# bazel_dep(name = "rules_apple", version = "3.4.0", repo_name = "build_bazel_rules_apple")
bazel_dep(name = "apple_support", version = "1.12.0", repo_name = "build_bazel_apple_support")

python = use_extension("@rules_python//python/extensions:python.bzl", "python")
python.toolchain(
    configure_coverage_tool = True,
    # Only set when you have mulitple toolchain versions.
    is_default = True,
    python_version = "3.12",
)

pip = use_extension("@rules_python//python/extensions:pip.bzl", "pip")
pip.parse(
    hub_name = "pip",
    python_version = "3.12",
    requirements_lock = "//:requirements_lock.txt",
)
use_repo(pip, "pip")

# python bindings
bazel_dep(name = "pybind11_bazel", version = "2.12.0")

# cpp lib
bazel_dep(name = "fmt", version = "10.2.1")
bazel_dep(name = "spdlog", version = "1.13.0")

# Tests frameworks
bazel_dep(name = "catch2", version = "3.5.4")
bazel_dep(name = "googletest", version = "1.14.0.bcr.1")

# benchmark
bazel_dep(name = "google_benchmark", version = "1.8.3")

# compile_comands.json generator
# Hedron's Compile Commands Extractor for Bazel
# https://github.com/hedronvision/bazel-compile-commands-extractor
bazel_dep(name = "hedron_compile_commands", dev_dependency = True)
git_override(
    module_name = "hedron_compile_commands",
    commit = "a14ad3a64e7bf398ab48105aaa0348e032ac87f8",
    remote = "https://github.com/hedronvision/bazel-compile-commands-extractor.git",
)

# toolchains for compilets and libs
# TODO(sokolik): add and using gcc

# bazel_dep(name = "toolchains_llvm", version = "1.0.0")

# bazel linter
bazel_dep(name = "buildozer", version = "7.1.1.1")
bazel_dep(name = "depend_on_what_you_use", version = "0.2.0")

# linters

bazel_dep(name = "bazel_clang_tidy", dev_dependency = True)
git_override(
    module_name = "bazel_clang_tidy",
    commit = "bff5c59c843221b05ef0e37cef089ecc9d24e7da",
    remote = "https://github.com/erenon/bazel_clang_tidy.git",
)

# TODO(sokolik): uncomment when it make possible
# bazel_dep(name = "bazel_clang_format", dev_dependency = True)
# git_override(
#     module_name = "bazel_clang_format",
#     remote = "https://github.com/oliverlee/bazel_clang_format.git",
#     commit = "ad5f2572518053b36a2d999f7b824fb5a7819ab4",
# )

# tar packaging
bazel_dep(name = "rules_pkg", version = "0.10.1")

