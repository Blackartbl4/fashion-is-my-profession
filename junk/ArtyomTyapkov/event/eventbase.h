#pragma once

#include <QTime>
#include <map>
enum class Weather{
    windy,
    clear,
    rainy,
    sunny,
    sandstormy,
    tsunamiy,
    snowy
};

enum class Mood{
    usual,
    cheerful,
    brash,
    mourning,
    sad
};

enum class Officiality{
    official,
    casual,
    festive,
    doingsport
};

std::map<std::string, Weather> weather_map {
    {"windy", Weather::windy},
    {"clear", Weather::clear},
    {"rainy", Weather::rainy},
    {"snowy", Weather::snowy},
    {"sandstormy", Weather::sandstormy},
    {"sunny", Weather::sunny},
    {"tsunamiy", Weather::tsunamiy}
};

std::map<std::string, Mood> mood_map {
    {"usual", Mood::usual},
    {"cheerflul", Mood::cheerful},
    {"brash", Mood::brash},
    {"mouring", Mood::mourning},
    {"sad", Mood::sad}
};

std::map<std::string, Officiality> officiality_map {
    {"official", Officiality::official},
    {"calual", Officiality::casual},
    {"festive", Officiality::festive},
    {"doingsport", Officiality::doingsport}
};

class Event
{
private:
    Officiality officiality;
    Mood mood;
    Weather weather;
    bool indoors;
    int temperature;
    QTime duration;
    QDateTime timestamp;
public:
    Event() = delete;
    Event(Officiality o, Mood m, Weather w, bool i, int t, QTime d, QDateTime dt) : officiality(o), mood(m), weather(w),
        indoors(i), temperature(t), duration(d), timestamp(dt) {
    }
    Event(Event& e) = default;
    Event(Event&& e) = default;
    Event& operator=(const Event& e) = default;
    Event& operator=(Event&& e) = default;
    ~Event() = default;
};

class EventStatics {
private:
    std::vector<Event> events;
};

