//
// Created by Pavel Kasila on 7.05.24.
//

#include "statistics_export_view.h"

#include "junk/pavelkasila/utils/csv.h"
#include "statistics_mode.h"

#include <QFile>
#include <QFileDialog>
#include <QPushButton>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QTextStream>
#include <QVBoxLayout>
#include <memory>

namespace outfit::core::statistics {
StatisticsExportView::StatisticsExportView(StatisticsMode mode) : mode_(mode) {
    setContentsMargins(0, 0, 0, 0);

    layout_manager_ = std::make_unique<QVBoxLayout>(this);
    export_button_ = std::make_unique<QPushButton>("Export");
    layout_manager_->addWidget(export_button_.get());

    connect(export_button_.get(), &QPushButton::clicked, this, &StatisticsExportView::exportCount);
}

void StatisticsExportView::setMode(StatisticsMode mode) {
    mode_ = mode;
}

QString StatisticsExportView::countDataQuery() {
    switch (mode_) {
        case StatisticsMode::Sizes:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.size_id = ic.id) as count from item_sizes ic";
        case StatisticsMode::Seasons:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.season_id = ic.id) as count from seasons ic";
        case StatisticsMode::Materials:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.material_id = ic.id) as count from item_materials ic";
        case StatisticsMode::Categories:
        default:
            return "select ic.id, ic.name, (select count(id) from items i "
                   "where i.category_id = ic.id) as count from item_categories ic";
    }
}

void StatisticsExportView::exportCount() {
    QSqlQuery query;
    query.prepare(countDataQuery());
    utils::csv::saveQuery("id,name,count", query);
}
}  // namespace outfit::core::statistics